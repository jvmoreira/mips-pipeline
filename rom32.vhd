library IEEE;
use IEEE.std_logic_1164.all;
use IEEE.std_logic_unsigned.all;
use work.p_MI0.all;

entity rom32 is
    port (
        address: in reg32;
        data_out: out reg32
    );
end rom32;

architecture arq_rom32 of rom32 is
    signal mem_offset: std_logic_vector(5 downto 0);
    signal address_select: std_logic;
begin
    mem_offset <= address(7 downto 2);
    add_sel: process(address)

    begin
        if address(31 downto 8) = x"000000" then
            address_select <= '1';
        else
            address_select <= '0';
        end if;
    end process;

    access_rom: process(address_select, mem_offset)
    begin
        if address_select = '1' then
            case mem_offset is
-- [[           $memoria
                when "000000" => data_out <= "00100000000000100000000000001100"; -- addi $2, $0, 12
                when "000001" => data_out <= "00100000000000010000000000000100"; -- addi $1, $0, 4
                when "000010" => data_out <= "10101100011000010000000000000000"; -- for: sw $1, 0($3)
                when "000011" => data_out <= "00100000001000010000000000000100"; -- addi $1, $1, 4
                when "000100" => data_out <= "00100000011000110000000000000100"; -- addi $3, $3, 4
                when "000101" => data_out <= "00010000001000100000000000000001"; -- beq $1, $2, fimFor
                when "000110" => data_out <= "00010000000000001111111111111011"; -- beq $0, $0, for
                when "000111" => data_out <= "10010100000001000000000000000000"; -- fimFor: lw $4, 0($0)
                when "001000" => data_out <= "00100000000001100000000000000001"; -- addi $6, $0, 1
                when "001001" => data_out <= "01001100100001010000000000000000"; -- lwdi $5, $4
                when "001010" => data_out <= "00000000101001100010100000100000"; -- add $5, $5, $6
-- ]]
                when others  => data_out <= (others => 'X');
            end case;
            end if;
    end process;
end arq_rom32;

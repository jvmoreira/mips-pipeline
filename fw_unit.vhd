library IEEE;
use ieee.std_logic_1164.all;
use work.p_MI0.all;

entity fw_unit is
       port(
		ID_Branch, EX_RegWrite, MEM_RegWrite, WB_RegWrite: in std_logic;
		EX_RegRd, MEM_RegRd, WB_RegRd, ID_rs, ID_rt, EX_rs, EX_rt: in std_logic_vector(4 downto 0);
                ID_ForwardA, ID_ForwardB: out std_logic_vector(1 downto 0);
             	EX_ForwardA, EX_ForwardB: out std_logic_vector(1 downto 0)
           );
end fw_unit;

architecture arq_fw_unit of fw_unit is

begin

    FW_EX: process (WB_RegWrite, MEM_RegWrite, MEM_RegRd, WB_RegRd, EX_rs, EX_rt) begin
        case WB_RegWrite is
            when '1' => if WB_RegRd /= "00000" then
                            if (MEM_RegRd /= EX_rs) and (WB_RegRd = EX_rs) then
                                EX_ForwardA <= "01";
                            else
                                EX_ForwardA <= "00";
                            end if;
                            if (MEM_RegRd /= EX_rt) and (WB_RegRd = EX_rt) then
                                EX_ForwardB <= "01";
                            else
                                EX_ForwardB <= "00";
                            end if;
                        end if;
            when others => EX_ForwardA <= "00"; EX_ForwardB <= "00";
        end case;

        case MEM_RegWrite is
            when '1' => if MEM_RegRd /= "00000" then
                            if MEM_RegRd = EX_rs then
                                EX_ForwardA <= "10";
                            end if;
                            if MEM_RegRd = EX_rt then
                                EX_ForwardB <= "10";
                            end if;
                        end if;
            when others => --EX_ForwardA <= "00"; EX_ForwardB <= "00";
        end case;
    end process;

    FW_ID: process (ID_Branch, EX_RegWrite, MEM_RegWrite, EX_RegRd, MEM_RegRd, ID_rs, ID_rt) begin
            case ID_Branch is
                when '1' => if (EX_RegWrite = '1') and (EX_RegRd /= "00000") then
                                if EX_RegRd = ID_rs then
                                    ID_ForwardA <= "01";
                                end if;
                                if EX_RegRd = ID_rt then
                                    ID_ForwardB <= "01";
                                end if;
                            elsif (MEM_RegWrite = '1') and (MEM_RegRd /= "00000") then
                                if (EX_RegRd /= ID_rs) and (MEM_RegRd = ID_rs) then
                                    ID_ForwardA <= "10";
                                else
                                    ID_ForwardA <= "00";
                                end if;
                                if (EX_RegRd /= ID_rt) and (MEM_RegRd = ID_rt) then
                                    ID_ForwardB <= "10";
                                else
                                    ID_ForwardB <= "00";
                                end if;
                            else
                                ID_ForwardA <= "00";
                                ID_ForwardB <= "00";
                            end if;

                when others => ID_ForwardA <= "00"; ID_ForwardB <= "00";
            end case;

    end process;

end arq_fw_unit;

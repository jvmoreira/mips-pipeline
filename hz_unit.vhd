library IEEE;
use ieee.std_logic_1164.all;
use work.p_MI0.all;

entity hz_unit is
       port(
		EX_MemRead, MEM_MemRead: in std_logic;
        ID_lwdi, MEM_lwdi: in std_logic;
		EX_rt, ID_rs, ID_rt : in std_logic_vector(4 downto 0);
             	PC_Write, IF_ID_En, ID_Stall: out std_logic
           );
end hz_unit;

architecture arq_hz_unit of hz_unit is

begin

    HZ_DETECTION: process(EX_MemRead, MEM_MemRead, ID_lwdi, MEM_lwdi, EX_rt, ID_rs, ID_rt) begin
		case EX_MemRead is
			when '1' => if (ID_lwdi /= '1') then
                            if (EX_rt = ID_rs) or (EX_rt = ID_rt) then -- Dependência
     							PC_Write <= '0';    -- Não permite a escrita no PC
                                IF_ID_En <= '0';    -- Não permite escrita no IF/ID Pipeline
                                ID_Stall <= '1';    -- Stall/No-op.
    						end if;
                        else -- Stall obrigatório quando ID_lwdi = '1'
                            PC_Write <= '0';
                            IF_ID_En <= '0';
                            ID_Stall <= '1';
    					end if;
			when others => PC_Write <= '1';
                            IF_ID_En <= '1';
                            ID_Stall <= '0';
		end case;
        case MEM_MemRead is
            when '1' => if MEM_lwdi = '1' then
                            if (EX_rt = ID_rs) or (EX_rt = ID_rt) then
                                PC_Write <= '0';
                                IF_ID_En <= '0';
                                ID_Stall <= '1';
                            end if;
                        end if;
            when others =>
        end case;
	end process;

end arq_hz_unit;

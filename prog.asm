# Test prog.asm - Edit your MIPS assembly here
# PS.: Some instructions below are not implemented in the processor datapath yet.
#     You will have to implement, for example: addi, andi, ldi, jal, j, jr. Uncomment
#     them only when their datapath structures and controls are available.


addi $2, $0, 12
addi $1, $0, 4
for:
sw $1, 0($3)
addi $1, $1, 4
addi $3, $3, 4
beq $1, $2, fimFor
beq $0, $0, for
fimFor:
lw $4, 0($0)
addi $6, $0, 1
lwdi $5, $4
add $5, $5, $6
